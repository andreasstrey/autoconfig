/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.andreasstrey.microserviceclient;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import de.andreasstrey.hazelcastdiscovery.ConfigurationInstance;
import de.andreasstrey.hazelcastdiscovery.ServiceConfiguration;
import java.util.Collection;
import java.util.Random;

/**
 *
 * @author andreasstrey
 */
public class CLient {

    public static void main(String[] args) throws UnirestException {
        Collection<ServiceConfiguration> configs = ConfigurationInstance.getConfigurations("MicroService");

        if (configs.size() > 0) {
            ServiceConfiguration config = configs.stream().skip(new Random().nextInt(configs.size())).findFirst().get();
            System.out.println(config.getPort());
            System.out.println(Unirest.get("http://" + config.getHost() + ":" + config.getPort()).asString().getBody());
        }
        ConfigurationInstance.shutdownHazelcastClient();
    }
}
