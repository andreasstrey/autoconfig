/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.andreas.strey.microserviceserver;

import com.google.inject.Binder;
import com.google.inject.Module;
import java.util.Arrays;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.annotation.WebListener;
import org.jboss.resteasy.plugins.guice.GuiceResteasyBootstrapServletContextListener;

/**
 *
 * @author andreasstrey
 */
@WebListener
public class GuiceListener extends GuiceResteasyBootstrapServletContextListener {

    @Override
    protected List<? extends Module> getModules(ServletContext context) {
        System.out.println("lama rama");
        return Arrays.asList((Module) (final Binder binder) -> {
            binder.bind(HelloWorld.class);
            binder.bind(Test.class).to(TestImpl.class);
        });
    }

}
