package de.andreas.strey.microserviceserver;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.Date;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

/**
 *
 * @author andreasstrey
 */
@Path("/")
@Singleton
public class HelloWorld {

    @Inject
    private Test test;

    private Date date;

    public HelloWorld() {
        System.out.println("test");
    }

    @GET
    public Response getHelloWorld() {
        String value = "Hello World";
        return Response.status(200).entity(test.getText()).build();
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
