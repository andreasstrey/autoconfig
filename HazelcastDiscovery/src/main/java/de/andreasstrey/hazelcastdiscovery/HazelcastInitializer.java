package de.andreasstrey.hazelcastdiscovery;

import java.io.IOException;
import java.util.Set;
import javax.servlet.GenericServlet;
import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.HandlesTypes;
import org.apache.commons.lang3.BooleanUtils;

/**
 *
 * @author andreasstrey
 */
@HandlesTypes(value = {})
public class HazelcastInitializer implements ServletContainerInitializer {

    @Override
    public void onStartup(Set<Class< ?>> set, ServletContext sc) throws ServletException {
        final boolean register = BooleanUtils.toBoolean(System.getProperty("service.register"));
        sc.addServlet("HazelcastInitialization", new GenericServlet() {

            @Override
            public void init() throws ServletException {
                super.init();
                if (register) {
                    ConfigurationInstance.registerService();
                }
            }

            @Override
            public void destroy() {
                super.destroy();
                if (register) {
                    ConfigurationInstance.unregisterService();
                }
                ConfigurationInstance.shutdownHazelcast();
            }

            @Override
            public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
                //nothing to do here. The servlet is only to populate or remove the service.
            }
        });
    }
}
