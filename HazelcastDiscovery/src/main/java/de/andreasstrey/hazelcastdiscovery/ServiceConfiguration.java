package de.andreasstrey.hazelcastdiscovery;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author andreasstrey
 */
public class ServiceConfiguration implements Serializable {

    private final String host;
    private final int port;
    private final String serviceName;

    public ServiceConfiguration(String host, int port, String serviceName) {
        this.host = host;
        this.port = port;
        this.serviceName = serviceName;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public String getServiceName() {
        return serviceName;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + Objects.hashCode(this.host);
        hash = 61 * hash + this.port;
        hash = 61 * hash + Objects.hashCode(this.serviceName);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ServiceConfiguration other = (ServiceConfiguration) obj;
        if (!Objects.equals(this.host, other.host)) {
            return false;
        }
        if (this.port != other.port) {
            return false;
        }

        return Objects.equals(this.serviceName, other.serviceName);
    }

    @Override
    public String toString() {
        return "ServiceConfiguration{" + "host=" + host + ", port=" + port + ", serviceName=" + serviceName + '}';
    }
}
