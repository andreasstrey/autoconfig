package de.andreasstrey.hazelcastdiscovery;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.impl.HazelcastClientProxy;
import com.hazelcast.config.Config;
import com.hazelcast.config.MapConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.instance.GroupProperties;
import java.util.Collection;
import java.util.Timer;
import java.util.TimerTask;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author andreasstrey
 */
public class ConfigurationInstance {

    private static final Logger LOG = LoggerFactory.getLogger(HazelcastInitializer.class);

    private static final ServiceConfiguration SERVICE_CONFIGURATION;

    static {
        int port = NumberUtils.toInt(System.getProperty("service.http.port"), 8080);
        String host = System.getProperty("service.host.name", "localhost");
        String serviceName = System.getProperty("service.name");
        SERVICE_CONFIGURATION = new ServiceConfiguration(host, port, serviceName);
    }

    private static final HazelcastInstance INSTANCE = initializeInstance();

    private ConfigurationInstance() {

    }

    public static Collection<ServiceConfiguration> getConfigurations(String serviceName) {
        IMap<String, ServiceConfiguration> configs = INSTANCE.getMap(serviceName);
        return configs.values();
    }

    public static void shutdownHazelcastClient() {
        if (INSTANCE.getClass().isAssignableFrom(HazelcastClientProxy.class)) {
            INSTANCE.shutdown();
        }
    }

    static void registerService() {
        LOG.info("initialize: {}", SERVICE_CONFIGURATION);

        Timer timer = new Timer(true);
        timer.schedule(new TimerTask() {

            @Override
            public void run() {
                IMap<String, ServiceConfiguration> configs = INSTANCE.getMap(SERVICE_CONFIGURATION.getServiceName());

                configs.put(SERVICE_CONFIGURATION.toString(), SERVICE_CONFIGURATION);
            }
        }, 0, 15000);
    }

    static void unregisterService() {
        LOG.info("destroy");

        IMap<String, ServiceConfiguration> configs = INSTANCE.getMap(SERVICE_CONFIGURATION.getServiceName());

        configs.remove(SERVICE_CONFIGURATION.toString());
    }

    static void shutdownHazelcast() {
        INSTANCE.shutdown();
    }

    private static HazelcastInstance initializeInstance() {
        boolean asServer = BooleanUtils
                .toBoolean(System.getProperty("hazelcast.instance.asServer"));
        if (asServer) {
            return createServcer();
        } else {
            return createClient();
        }
    }

    private static HazelcastInstance createClient() {
        LOG.info("create Hazelcast instance as client");
        return HazelcastClient.newHazelcastClient();
    }

    private static HazelcastInstance createServcer() {
        LOG.info("create Hazelcast instance as server");
        Config config = new Config();
        config.setProperty(GroupProperties.PROP_SHUTDOWNHOOK_ENABLED, "false");

        MapConfig mapConfig = new MapConfig(SERVICE_CONFIGURATION.getServiceName());
        mapConfig.setTimeToLiveSeconds(15);
        config.addMapConfig(mapConfig);

        return Hazelcast.newHazelcastInstance(config);
    }
}
